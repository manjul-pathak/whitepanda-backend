var nodemailer = require('nodemailer');
var mg = require('nodemailer-mailgun-transport');
var jwt = require('jsonwebtoken');
var emailTemplate = require('../lib/emailTemplete');
let validateRequest = require("../utility/validateRequest");
let request = require('request');


// This is your API key that you retrieve from www.mailgun.com/cp (free up to 10K monthly emails)
var auth = {
  auth: {
      api_key: 'key-bd19fffa48a8d16cd7ef78c72c670f37',
      domain: 'mail.opeyy.com'
  }
}

var nodemailerMailgun = nodemailer.createTransport(mg(auth));

exports.verifyOtp = async (otp, details) => {
  return new Promise((resolve, reject) => {
    request(`https://2factor.in/API/V1/b04a0d89-d0d0-11e9-ade6-0200cd936042/SMS/VERIFY/${details}/${otp}`, (error, res, body) => {
      if (!error && res.statusCode == 200) {
        resolve(body);
      } else {
        reject(error);
      }
    })
  })
};

exports.sendOtp = async (phone_number) => {
  return new Promise((resolve, reject) => {
    request(`https://2factor.in/API/V1/b04a0d89-d0d0-11e9-ade6-0200cd936042/SMS/+91${phone_number}/AUTOGEN`, (error, res, body) => {
      if (!error && res.statusCode == 200) {
        resolve(body);
      } else {
        reject(res);
      }
    })
  })
};


sendEmail = async (email) => {
  return new Promise((resolve, reject) => {
    let token = jwt.sign({ username: email }, 'tempsecretcode');
    var verifyUrl = "http://localhost:3000/verify-email?token=" + token;
    nodemailerMailgun.sendMail({
        from: 'no-reply@opeyy.com',
        to: email, // An array if you have multiple recipients.
        subject: 'Verification Email for your Whitepanda account!',
        html: emailTemplate.verificationEmail(verifyUrl),
    }, function (err, info) {
        if (err) {
          reject(err);
        }
        else {
          resolve(info);
        }
    });
  })
};

module.exports.sendEmail = sendEmail;