var port = process.env.PORT;
var ip = process.env.IP;
var restify = require('restify');
const corsMiddleware = require("restify-cors-middleware");
var mongoose = require('mongoose');
var mongojs = require('mongojs');
var db = mongojs("mongodb://admin:admin123@ds235768.mlab.com:35768/whitepanda", ['login', 'userList', 'likes', 'reports', 'user', 'deviceID', 'preference', 'credit', 'block', 'chat']);

var server = restify.createServer();

server.use(restify.plugins.acceptParser(server.acceptable));
server.use(restify.plugins.queryParser());
server.use(restify.plugins.bodyParser());

const cors = corsMiddleware({
    preflightMaxAge: 5, //Optional
    origins: ["http://localhost:3000"],
    allowHeaders: ["API-Token", "Authorization"],
    exposeHeaders: ["API-Token-Expiry"]
  });
  
server.pre(cors.preflight);
server.use(cors.actual);

// CORS
server.use(function(req, res, next) {
    res.header('Access-Control-Allow-Credentials', 'true');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    next();
});

server.listen(8000, function() {
    console.log("Server started @ ", 8000);
});

var login = require('./controller/login')(server, db);
var verifyOtp = require('./controller/otpVerification')(server, db);
var sendEmail = require('./controller/sendEmail')(server, db);
var register = require('./controller/register')(server, db);