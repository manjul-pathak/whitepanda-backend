let validateRequest = require("../utility/validateRequest");
let request = require('request');
var loginHelper = require('../lib/loginHelper');
let pwdManager = require('../utility/managePassword');
var jwt = require('jsonwebtoken');

module.exports = (server, db) => {
    server.post('/api/v1/otp-verify', async (req, res, next) => {
        var result = {};
        let response = await loginHelper.verifyOtp(req.body.otp, req.body.details)
        .catch((e) => {
            res.writeHead(403, {
                'Content-Type': 'application/json; charset=utf-8'
            });
            res.end(JSON.stringify({
                error: "Invalid OTP",
                type: 'OTP_ERROR'
            }));
        });
        response = JSON.parse(response);
        if (response.Status == "Success") {
            result.status = "success";
            result.data = {};
            result.data.details = response.Details;
            result.data.phone = req.body.phone;
            result.data.token = jwt.sign({ username: req.body.phone }, 'mysecretkey');
            res.writeHead(200, {
                "Content-Type": "application/json; charset=utf-8"
            });
            res.end(JSON.stringify(result));
        } else{
            res.writeHead(403, {
                'Content-Type': 'application/json; charset=utf-8'
            });
            res.end(JSON.stringify({
                error: "Invalid OTP",
                type: 'OTP_ERROR'
            }));
        }
        return next();
    });
};