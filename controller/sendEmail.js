var nodemailer = require('nodemailer');
var mg = require('nodemailer-mailgun-transport');
var jwt = require('jsonwebtoken');
var emailTemplate = require('../lib/emailTemplete');
let validateRequest = require("../utility/validateRequest");


// This is your API key that you retrieve from www.mailgun.com/cp (free up to 10K monthly emails)
var auth = {
    auth: {
        api_key: 'key-bd19fffa48a8d16cd7ef78c72c670f37',
        domain: 'mail.opeyy.com'
    }
}

var nodemailerMailgun = nodemailer.createTransport(mg(auth));

module.exports = function (server, db) {
    server.get('/api/v1/send-email', function (req, res, next) {
        let token = jwt.sign({ username: req.query.email }, 'tempsecretcode');
        var verifyUrl = "http://localhost:3000/reset-password?token=" + token;
        nodemailerMailgun.sendMail({
            from: 'no-reply@opeyy.com',
            to: req.query.email, // An array if you have multiple recipients.
            subject: 'Password reset request for your Whitepanda account!',
            html: emailTemplate.passwordReset(verifyUrl),
        }, function (err, info) {
            if (err) {
                console.log('Error: ' + err);
            }
            else {
                console.log('Response: ' + info);
            }
        });
        return next();
    });

    server.get("/api/v1/email-verify", function (req, res, next) {
        var result = {};
        jwt.verify(req.query.token, 'tempsecretcode', function (err, decoded) {
            if (err) {
                //can't verify the email 
                result.status = "fail";
                result.message = "Email verified failed";
                res.writeHead(403, {
                    "Content-Type": "application/json; charset=utf-8"
                });
                res.end(JSON.stringify(result));
            }
            else {
                //email verified successfully
                db.user.update(
                    { email: decoded.username },
                    { $set: { "isVerified": true}, },
                    function(error, _res){
                        if(error){
                            result.status = "fail";
                            result.message = "Some error occured";
                            res.writeHead(403, {
                                "Content-Type": "application/json; charset=utf-8"
                            });
                            res.end(JSON.stringify(result));
                        } else{
                            result.status = "success";
                            result.message = "Email verified successfully";
                            res.writeHead(200, {
                                "Content-Type": "application/json; charset=utf-8"
                            });
                            res.end(JSON.stringify(result));
                        }
                    });
            }
        });
        return next();
    });
}