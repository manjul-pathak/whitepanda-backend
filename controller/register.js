let validateRequest = require("../utility/validateRequest");
let request = require('request');
var loginHelper = require('../lib/loginHelper');
let pwdManager = require('../utility/managePassword');
var jwt = require('jsonwebtoken');

module.exports = (server, db) => {
    
    server.post('/api/v1/register', function (req, res, next) {
        var result = {};
        db.user.find({
            username: req.body.username
        }, function (err, data) {
            if (data[0] != undefined) {
                res.writeHead(403, {
                    'Content-Type': 'application/json; charset=utf-8'
                });
                res.end(JSON.stringify({
                    error: "Email is already registered",
                    type: 'EMAIL_ERROR'
                }));
            } else {
                pwdManager.cryptPassword(req.body.password, function (err, hash) {
                    db.user.insert({ username: req.body.username, password: hash, provider: "EMAIL" },
                        function (err, data) {
                            if (err) { // duplicate key error
                                if (err.code == 11000) /* http://www.mongodb.org/about/contributors/error-codes/*/ {
                                    res.writeHead(400, {
                                        'Content-Type': 'application/json; charset=utf-8'
                                    });
                                    res.end(JSON.stringify({
                                        error: err,
                                        message: "A user with this email already exists"
                                    }));
                                }
                            } else {
                                var _emailRes =  loginHelper.sendEmail(req.body.username);
                                res.writeHead(200, {
                                    'Content-Type': 'application/json; charset=utf-8'
                                });
                                result.status = "success";
                                result.data = {};
                                result.data.token = jwt.sign({ username: req.body.username }, 'mysecretkey');
                                res.end(JSON.stringify(result));
                            }
                        });
                });
            }
        });
        return next();
    });
};