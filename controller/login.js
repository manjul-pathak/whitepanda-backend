let validateRequest = require("../utility/validateRequest");
let request = require('request');
var loginHelper = require('../lib/loginHelper');
let pwdManager = require('../utility/managePassword');
var jwt = require('jsonwebtoken');

module.exports = (server, db) => {

    server.post('/api/v1/login', async (req, res, next) => {
        var result = {};
        if (req.body.provider == 'EMAIL') {
            db.user.find({ username: req.body.email }, function (error, response) {
                if (error) {
                    result.status = "fail";
                    result.message = "Some error occured";
                    res.writeHead(403, {
                        "Content-Type": "application/json; charset=utf-8"
                    });
                    res.end(JSON.stringify(result));
                } else {
                    if (response.length === 0) {
                        result.status = "fail";
                        result.type = "EMAIL_ERROR";
                        result.message = "Email is not registered";
                        res.writeHead(403, {
                            "Content-Type": "application/json; charset=utf-8"
                        });
                        res.end(JSON.stringify(result));
                    } else {
                        pwdManager.comparePassword(req.body.password, response[0].password, function (error, isPasswordMatch) {
                            if (isPasswordMatch) {
                                res.writeHead(200, {
                                    'Content-Type': 'application/json; charset=utf-8'
                                });
                                result.status = "success";
                                result.data = {};
                                result.data.token = jwt.sign({ username: req.body.username }, 'mysecretkey');
                                res.end(JSON.stringify(result));
                            } else {
                                res.writeHead(403, {
                                    'Content-Type': 'application/json; charset=utf-8'
                                });
                                res.end(JSON.stringify({
                                    type: "ERROR",
                                    status: "fail",
                                    message: "Invalid Email or Password"
                                }));
                            }
                        })
                    }
                }
            })
        } else {
            let response = await loginHelper.sendOtp(req.body.phone);
            response = JSON.parse(response);
            if (response.Status == "Success") {
                db.user.find({username: req.body.phone}, function(err, _res){
                    if(err){
                        result.status = "fail";
                        result.data = {};
                        result.data.message = "Some error occured";
                        res.writeHead(403, {
                            "Content-Type": "application/json; charset=utf-8"
                        });
                        res.end(JSON.stringify(result));                  
                    } else{
                        if(_res.length > 0){
                            result.status = "success";
                            result.data = {};
                            result.data.details = response.Details;
                            result.data.phone = req.body.phone;
                            res.writeHead(200, {
                                "Content-Type": "application/json; charset=utf-8"
                            });
                            res.end(JSON.stringify(result));
                        } else{
                            db.user.insert({
                                username: req.body.phone,
                                provider: "PHONE"
                            }, function(err, _result){
                                if(err){
                                    result.status = "fail";
                                    result.data = {};
                                    result.data.message = "Some error occured";
                                    res.writeHead(403, {
                                        "Content-Type": "application/json; charset=utf-8"
                                    });
                                    res.end(JSON.stringify(result));   
                                } else{
                                    result.status = "success";
                                    result.data = {};
                                    result.data.details = response.Details;
                                    result.data.phone = req.body.phone;
                                    res.writeHead(200, {
                                        "Content-Type": "application/json; charset=utf-8"
                                    });
                                    res.end(JSON.stringify(result));
                                }
                            })
                        }
                    }
                })
            }
        }
        return next();
    });

    server.post('/api/v1/reset-password', function (req, res, next) {
        jwt.verify(req.body.token, 'tempsecretcode', function(err, decoded) {
            var result = {};
            if (req.body.password.trim().length == 0) {
                res.writeHead(403, {
                    'Content-Type': 'application/json; charset=utf-8'
                });
                res.end(JSON.stringify({
                    error: "Invalid Credentials"
                }));
            } else {
                pwdManager.cryptPassword(req.body.password, function (err, hash) {
                    if(err){
                        result.status = "fail";
                        result.message = "Some error occured";
                        res.writeHead(403, {
                            "Content-Type": "application/json; charset=utf-8"
                        });
                        res.end(JSON.stringify(result));
                    } else{
                    db.user.update(
                        { username: decoded.username },
                        { $set: { "password": hash } }, function (error, resp) {
                            if (error) {
                                result.status = "fail";
                                result.message = "Some error occured";
                                res.writeHead(403, {
                                    "Content-Type": "application/json; charset=utf-8"
                                });
                                res.end(JSON.stringify(result));
                            } else {
                                result.status = "success";
                                result.message = "Password changed successfully";
                                res.writeHead(200, {
                                    "Content-Type": "application/json; charset=utf-8"
                                });
                                res.end(JSON.stringify(result));
                            }
                        });
                    }
                });
            }
        });
        return next();
    });
};