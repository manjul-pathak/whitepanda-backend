var jwt = require('jsonwebtoken');
 
module.exports.validate = function (req, res, next) {
    if (!req.headers.authorization) {
        res.writeHead(403, {
            'Content-Type': 'application/json; charset=utf-8'
        });
        res.end(JSON.stringify({
            error: "You are not authorized to access this application",
            message: "A token is required as part of the header"
        }));
    }
    else{
            jwt.verify(req.headers.authorization, 'tempsecretcode', function(err, decoded) {      
            if (err) {
                return res.json({ success: false, message: 'Failed to authenticate token.' });    
            } 
            else {
                if(req.body === undefined || req.body.length == 0){
                    req.body = {};
                } 
                req.body.username = decoded.username;
                return next();
          }
        });
    }
};